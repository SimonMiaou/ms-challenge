# frozen_string_literal: true

require 'test_helper'

require 'web_app'

describe WebApp do
  include Rack::Test::Methods

  def app
    WebApp
  end

  def setup
    ENV['CONTENTFUL_SPACE'] = 'your_contentful_space'
    ENV['CONTENTFUL_ACCESS_TOKEN'] = 'your_contentful_access_token'
  end

  describe '/' do
    it 'list the recipes' do
      stub_request(:get, 'https://cdn.contentful.com/spaces/your_contentful_space/environments/master/entries?content_type=recipe')
        .with(headers: { 'Authorization' => 'Bearer your_contentful_access_token' })
        .to_return(status: 200,
                   body: File.new('spec/fixtures/cdn.contentful.com/spaces/spacename/environments/master/entries.json'),
                   headers: {})

      get '/'

      _(last_response.body).must_include(
        '<a href="/recipe/4dT8tcb6ukGSIg2YyuGEOm">White Cheddar Grilled Cheese with Cherry Preserves &amp; Basil</a>'
      )
      _(last_response.body).must_include(
        '<a href="/recipe/5jy9hcMeEgQ4maKGqIOYW6">Tofu Saag Paneer with Buttery Toasted Pita</a>'
      )
      _(last_response.body).must_include(
        '<a href="/recipe/2E8bc3VcJmA8OgmQsageas">Grilled Steak &amp; Vegetables with Cilantro-Jalapeño Dressing</a>'
      )
      _(last_response.body).must_include(
        "<a href=\"/recipe/437eO3ORCME46i02SeCW46\">Crispy Chicken and Rice\twith Peas &amp; Arugula Salad</a>"
      )
    end
  end

  describe '/recipe/:recipe_id' do
    it 'show the attributes of the recipe' do
      stub_request(:get, 'https://cdn.contentful.com/spaces/your_contentful_space/environments/master/entries?sys.id=4dT8tcb6ukGSIg2YyuGEOm')
        .with(headers: { 'Authorization' => 'Bearer your_contentful_access_token' })
        .to_return(status: 200,
                   body: File.new('spec/fixtures/cdn.contentful.com/spaces/spacename/environments/master/entries_4dT8tcb6ukGSIg2YyuGEOm.json'),  # rubocop:disable Layout/LineLength
                   headers: {})

      get '/recipe/4dT8tcb6ukGSIg2YyuGEOm'

      _(last_response.body).must_include('<h1>White Cheddar Grilled Cheese with Cherry Preserves &amp; Basil</h1>')
    end
  end
end
