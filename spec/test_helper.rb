# frozen_string_literal: true

ENV['RACK_ENV'] = 'test'

require 'minitest/autorun'

require 'byebug'
require 'rack/test'
require 'webmock/minitest'
