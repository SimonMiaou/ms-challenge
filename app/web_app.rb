# frozen_string_literal: true

require 'contentful'
require 'kramdown'
require 'sinatra'

class WebApp < Sinatra::Base
  get '/' do
    @recipes = contentful.entries(content_type: 'recipe')

    slim :index
  end

  get '/recipe/:recipe_id' do |recipe_id|
    @recipe = contentful.entry(recipe_id)

    slim :recipe
  end

  private

  def contentful
    @contentful ||= Contentful::Client.new(space: ENV['CONTENTFUL_SPACE'],
                                           access_token: ENV['CONTENTFUL_ACCESS_TOKEN'])
  end
end
