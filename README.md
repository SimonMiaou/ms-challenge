# Challenge

## Run the app

```bash
# Clone the git repository
git clone git@gitlab.com:SimonMiaou/ms-challenge.git
cd ms-challenge

# Install gems
bundle install

# Before starting the server, you'll need to set the following environment variable
export CONTENTFUL_SPACE=your_contentful_space
export CONTENTFUL_ACCESS_TOKEN=your_contentful_access_token

# Start the web server
bundle exec rackup

# Server should run on http://localhost:9292/
```

## Testing the app

```bash
# Run tests
bundle exec rake test

# Run rubocop
bundle exec rubocop
```
